# NÃO CONSEGUI FIXAR UM ERRO NA COMUNICAÇÃO DO BANQUEV_API COM BANQUEV_DB
# PRA ISSO EXECUTO O BANQUEV_API DIRETO NA MAO, COMUNICANDO COM OS CONTAINERS DO PGADMIN4 E POSTGRESQL

- `docker-compose up -d --build`
- `python3 -m venv .venv`
- `source .venv/bin/activate`
- `pip3 install -r src/requirements.txt`
- `uvicorn app.main:app --reload`





Para execução do projeto rodar o seguinte comando

- `docker-compose up -d --build`

Após a execução estará disponívels 3 containers necessários para a aplicação

- `api 	        localhost:8000/api      localhost:8000/docs`
- `postgresql	localhost:5432`
- `pgAdmin4     localhost:5555`

Usuário default criado (Analista de Pedidos)

- `username/email=order_analyst@banquev.com`
- `password=user123`

Configurações pgadmin4

    - `email=email@banquev.com`
    - `user=root`
    - `password=root`
    - `database=banquev`
    - `host=banquev_db`



# 🧪 Backend Assessment

## Objetivo

O objetivo do teste é avaliar *hardskills* relacionadas ao desenvolvimento de *webservers* (especificamente) em `python`


## Problema

A cervejaria Banquev é uma nova cervejaria na cidade e, para entrar no mercado, decidiu praticar uma forma de venda consignada: fecha acordos com uma carteira de clientes na cidade de Uberlândia em que periodicamente serão entregues engradados de sua IPA para estabelecimentos de potenciais vendas. No entanto, a atual gestão da operação em Excel limita a profundidade da análise, exploração e utilização dos dados gerados

A gestão julga que investimentos em TI para sistematizar são essenciais para o alavancamento do empreendimento. Contrataram a T10 para desenvolver uma solução

## Escopo

Se usa:

- `python 3+`
- `postgresql 9.4+`
- `RESTful` protocol


### Personas

1. Analista de Pedidos
2. Analista Logístico
2. Administrador

### Use Cases

1. O analista de pedidos se autentica na plataforma com e-mail e senha

**Condição**: O usuário está logado e é um analista de pedidos

2. O usuário consegue criar, ver, atualizar e remover um produto
3. O usuário consegue criar, ver, atualizar e remover um cliente
4. O analista de pedidos consegue registrar um estoque de um cliente

    Um estoque representa um conjunto de produtos e uma quantidade associada à cada produto


### Diagrama de relacionamentos

![alt text](img/relationship.png "High level relationships")


## Entrega

A *Release* 0.1 🚀 consiste na implementação de um webserver capaz de fornecer controladores para os 4 *use cases* listados acima. Os atributos, esquemas e validações são arbitrários

Artefatos:

1. O script de criação de base **caso a base seja criada através da execução direta dos DDLs**

#### Bonus points if

1. Diagrama entidade relacionamento


## Critérios

O que será avaliado:

1. Arquitetura do webserver
2. Arquitetura do esquema da base de dados
3. Correção (*correctness*) da solução

    3.1 Os testes unitários

4. Padrões de segurança
5. Documentação (README, etc)

    5.1 Legibilidade

6. Manutenabilidade
7. *Style*, *naming*, etc
8. (**Se usar**) O uso correto do framekwork

#### Bonus points if

1. *Static type-checking*
2. Uso de *issue tracker* na gestão de atividades
3. Uso de *gitflow* como workflow de colaboração
4. *Deployment*
5. Boas práticas de modelagem de dados
    
    **Sugestão**: [Heroku](https://signup.heroku.com/login)

## Submissão

O candidato deverá implementar a solução e abrir um *merge request* para este repositório com a solução

Em etapas:

1. Faça um *fork* desse repositório (e não clonar direto)

    **O fork deve ser público**

2. Use o *fork* como repositório para escrever o código de submissão
3. Abra um *merge request* para este repositório

Feito 🤘


## Sugestões


O teste é livre e pode ser utilizado qualquer *lib* ou *framework*. Copiar ou "se inspirar" em código alheio é *veementemente* vetado 👈

Dito isso, essas referências podem ajudar:

1. webserver: [fastapi](https://github.com/tiangolo/fastapi), [flask](https://github.com/pallets/flask), [pyramid](https://github.com/Pylons/pyramid)
2. orm: [sqlalchemy](https://github.com/sqlalchemy), [flask-alchemy](https://github.com/pallets/flask-sqlalchemy)
3. data validation: [pydantic](https://github.com/samuelcolvin/pydantic)
4. static typing: [mypy](https://github.com/python/mypy)
