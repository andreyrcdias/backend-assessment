from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.product import Product
from app.data.schema.product import ProductCreate, ProductUpdate

class ProductService(AbstractService[Product, ProductCreate, ProductUpdate]):
    pass    

product_service = ProductService(Product)