from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.attendance import Attendance
from app.data.schema.attendance import AttendanceCreate, AttendanceUpdate

class AttendanceService(AbstractService[Attendance, AttendanceCreate, AttendanceUpdate]):
    pass        

attendance_service = AttendanceService(Attendance)