from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.permission import Permission
from app.data.schema.permission import PermissionCreate, PermissionUpdate

class PermissionService(AbstractService[Permission, PermissionCreate, PermissionUpdate]):
    pass

permission_service = PermissionService(Permission)