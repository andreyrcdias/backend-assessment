from app.service.abstract_service import AbstractService
from app.data.model.user_group import UserGroup
from app.data.schema.user_group import UserGroupCreate, UserGroupUpdate

class UserGroupService(AbstractService[UserGroup, UserGroupCreate, UserGroupUpdate]):
    pass

user_group_service = UserGroupService(UserGroup)