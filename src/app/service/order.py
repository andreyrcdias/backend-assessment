from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.order import Order
from app.data.schema.order import OrderCreate, OrderUpdate

class OrderService(AbstractService[Order, OrderCreate, OrderUpdate]):
    pass

order_service = OrderService(Order)