from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.catalog import Catalog
from app.data.schema.catalog import CatalogCreate, CatalogUpdate

class CatalogService(AbstractService[Catalog, CatalogCreate, CatalogUpdate]):
    pass

catalog_service = CatalogService(Catalog)