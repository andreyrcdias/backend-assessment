from typing import List, Optional
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.user import User
from app.data.schema.user import UserCreate, UserUpdate
from app.security.util import get_password_hash, verify_password

class UserService(AbstractService[User, UserCreate, UserUpdate]):
    
    def save_hashed_pass(self, db_session: Session, *, obj_input=UserCreate) -> User:
        db_obj=User(
            name=obj_input.name,
            email=obj_input.email,
            password=get_password_hash(obj_input.password)
        )
        db_session.add(db_obj)
        db_session.commit()
        db_session.refresh(db_obj)
        return db_obj

    def get_by_email(self, db_session: Session, *, email: str) -> Optional[User]:
        return db_session.query(User).filter(User.email == email).first()

    def authenticate(self, db_session: Session, *, email: str, password: str) -> Optional[User]:
        user = self.get_by_email(db_session, email=email)
        if not user:
            return None
        if not verify_password(password, user.password):
            return None
        return user


user_service = UserService(User)