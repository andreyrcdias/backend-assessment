from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.stock import Stock
from app.data.schema.stock import StockCreate, StockUpdate

class StockService(AbstractService[Stock, StockCreate, StockUpdate]):
    pass    

stock_service = StockService(Stock)