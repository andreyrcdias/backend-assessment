from typing import List
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.service.abstract_service import AbstractService
from app.data.model.customer import Customer
from app.data.schema.customer import CustomerCreate, CustomerUpdate

class CustomerService(AbstractService[Customer, CustomerCreate, CustomerUpdate]):
    pass

customer_service = CustomerService(Customer)