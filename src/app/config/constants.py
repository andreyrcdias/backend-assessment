import os

APP_NAME="backend-assessment"
API_PREFIX_CONTEXT="/api"
SECRET_KEY=os.urandom(32)
ALGORITHM="HS256"
ACCESS_TOKEN_EXPIRE_MINUTES=60 * 24 * 8 
DATABASE_URL="postgresql://root:root@127.0.0.1:5432/banquev"
