from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from app.database.session import engine, Session
from app.controller.routes import api_router
from app.config.constants import APP_NAME, API_PREFIX_CONTEXT
from app.database.initial_insert import init_db

db_initiated=False
app = FastAPI(title=APP_NAME)
app.include_router(api_router, prefix=API_PREFIX_CONTEXT)
if db_initiated==False:
    init_db()    
    db_initiated=True

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = Session()
    response = await call_next(request)
    request.state.db.close()
    return response
