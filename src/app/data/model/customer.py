from sqlalchemy import Boolean, Column, Integer, String, Table, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base

class Customer(Base):
    id = Column(Integer, primary_key=True)
    cnpj = Column(String, unique=True, nullable=False)
    name = Column(String, nullable=False)
    last_name = Column(String)
    email = Column(String, nullable=False)
    address = Column(String)
    phone = Column(String, nullable=False)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)

    orders = relationship("Order", back_populates="customer")
    catalogs = relationship("Catalog", back_populates="customer")
    stocks = relationship("Stock", back_populates="customer")