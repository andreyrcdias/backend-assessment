from sqlalchemy import Boolean, Column, Integer, String,  DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship, validates
from app.database.base import Base

class User(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    email = Column(String, unique=True, nullable=False, index=True)
    password = Column(String, nullable=False)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    attendances = relationship("Attendance", back_populates="user")
    user_groups = relationship("UserGroup")

    @validates("email")
    def validate_email(self, key, email):
        assert '@' in email
        return email