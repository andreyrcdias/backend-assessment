from sqlalchemy import Boolean, Column, Integer, String, Table, ForeignKey, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base

class Stock(Base):
    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    customer_id = Column(Integer, ForeignKey("customer.id"))
    customer = relationship("Customer", back_populates="stocks")
    products = relationship("Product", back_populates="stock")