from sqlalchemy import Boolean, Column, Integer, String, Table, Float, ForeignKey, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base
from app.data.model.order import Order
from app.data.model.catalog import Catalog

class Product(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, index=True)
    number = Column(String, unique=True, nullable=False, index=True)
    description = Column(String)
    price = Column(Float, nullable=False)
    quantity = Column(Integer, nullable=False)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    
    order_id = Column(Integer, ForeignKey(Order.id))
    order = relationship("Order", back_populates="products")

    catalog_id = Column(Integer, ForeignKey(Catalog.id))
    catalog = relationship("Catalog", back_populates="products")

    stock_id = Column(Integer, ForeignKey("stock.id"))
    stock = relationship("Stock", back_populates="products")