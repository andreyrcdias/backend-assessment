from sqlalchemy import Boolean, Column, Integer, String, Table, DateTime, Float, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base
from app.data.enum.order_status import OrderStatus
from app.data.model.attendance import Attendance

class Order(Base):
    id = Column(Integer, primary_key=True)
    number = Column(String, unique=True, nullable=False, index=True)
    description = Column(String)
    total_price = Column(Float, nullable=False, default=0)    
    status = Column(String, nullable=False, default=OrderStatus.DELIVERED.value)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    
    products = relationship("Product", back_populates="order")
    
    customer_id = Column(Integer, ForeignKey("customer.id"))
    customer = relationship("Customer", back_populates="orders")
    
    attendance_id = Column(Integer, ForeignKey(Attendance.id))
    attendance = relationship("Attendance", back_populates="orders")