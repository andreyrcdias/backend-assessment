from sqlalchemy import Boolean, Column, Integer, String, Table, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base

class Permission(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    user_groups = relationship("UserGroup", back_populates="permission")