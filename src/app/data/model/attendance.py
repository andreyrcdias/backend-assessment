from sqlalchemy import Boolean, Column, Integer, DateTime, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base
from app.data.model.user import User

class Attendance(Base):
    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)

    orders = relationship("Order", back_populates="attendance")

    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship("User", back_populates="attendances")