from sqlalchemy import Boolean, Column, Integer, String, Table, ForeignKey, DateTime
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from app.database.base import Base

class UserGroup(Base):
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    created_date = Column(DateTime, default=func.now())
    is_active = Column(Boolean, default=True)
    
    permission_id = Column(Integer, ForeignKey("permission.id"))
    permission = relationship("Permission", back_populates="user_groups")

    user_id = Column(Integer, ForeignKey("user.id"))
    user = relationship("User", back_populates="user_groups")