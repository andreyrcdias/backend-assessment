import enum

class OrderStatus(enum.Enum):
    OPEN = 'OPEN'
    PREPARATION = 'PREPARATION'
    DELIVERED = 'DELIVERED'
    FINISHED = 'FINISHED'