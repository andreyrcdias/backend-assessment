from typing import Optional
from pydantic import BaseModel

class OrderBase(BaseModel):
    cnpj: str
    name: str
    last_name: Optional[str] = None
    email: str
    address: Optional[str]
    phone: str    
    is_active: bool = None

class OrderCreate(OrderBase):
    attendance_id: Optional[int] = None
    customer_id: Optional[int] = None
    pass

class OrderUpdate(OrderBase):
    pass

class OrderBaseInDB(OrderBase):
    id: int

    class Config:
        orm_mode = True
        
class Order(OrderBaseInDB):
    pass

class OrderInDB(OrderBaseInDB):
    pass