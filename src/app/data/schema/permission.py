from typing import Optional, List
from pydantic import BaseModel

class PermissionBase(BaseModel):
    name: str
    description: Optional[str] = None
    is_active: Optional[bool] = None

class PermissionCreate(PermissionBase):
    pass

class PermissionUpdate(PermissionBase):
    pass

class PermissionBaseInDB(PermissionBase):
    id: int
    name: str

    class Config:
        orm_mode = True
        
class Permission(PermissionBaseInDB):
    pass

class PermissionInDB(PermissionBaseInDB):
    pass