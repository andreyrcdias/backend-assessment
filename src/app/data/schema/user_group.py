from typing import Optional
from pydantic import BaseModel

class UserGroupBase(BaseModel):
    name: str
    is_active: bool = None

class UserGroupCreate(UserGroupBase):
    user_id: Optional[int] = None
    permission_id: Optional[int] = None

class UserGroupUpdate(UserGroupCreate):
    pass

class UserGroupBaseInDB(UserGroupBase):
    id: int
    name: str
    permission_id: int
    user_id: int

    class Config:
        orm_mode = True
        
class UserGroup(UserGroupBaseInDB):
    pass

class UserGroupInDB(UserGroupBaseInDB):
    pass