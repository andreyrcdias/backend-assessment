from typing import Optional
from pydantic import BaseModel

class CatalogBase(BaseModel):
    name: str
    description: Optional[str] = None
    is_active: Optional[bool] = None

class CatalogCreate(CatalogBase):
    customer_id: Optional[int] = None

class CatalogUpdate(CatalogBase):
    pass

class CatalogBaseInDB(CatalogBase):
    id: int

    class Config:
        orm_mode = True
        
class Catalog(CatalogBaseInDB):
    pass

class CatalogInDB(CatalogBaseInDB):
    pass