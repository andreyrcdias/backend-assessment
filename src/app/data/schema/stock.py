from typing import Optional
from pydantic import BaseModel

class StockBase(BaseModel):
    name: str
    is_active: bool = None

class StockCreate(StockBase):
    customer_id: Optional[int] = None   

class StockUpdate(StockBase):
    pass

class StockBaseInDB(StockBase):
    id: int

    class Config:
        orm_mode = True
        
class Stock(StockBaseInDB):
    pass

class StockInDB(StockBaseInDB):
    pass