from typing import Optional
from pydantic import BaseModel

class ProductBase(BaseModel):
    name: str
    number: str
    description: Optional[str] = None
    price: float
    quantity: int
    is_active: Optional[bool] = None

class ProductCreate(ProductBase):
    order_id: Optional[int] = None
    catalog_id: Optional[int] = None
    stock_id: Optional[int] = None

class ProductUpdate(ProductBase):
    pass

class ProductBaseInDB(ProductBase):
    id: int

    class Config:
        orm_mode = True
        
class Product(ProductBaseInDB):
    pass

class ProductInDB(ProductBaseInDB):
    pass