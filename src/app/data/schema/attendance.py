from typing import Optional
from pydantic import BaseModel

class AttendanceBase(BaseModel):
    observation: Optional[str] = None
    is_active: Optional[bool] = None

class AttendanceCreate(AttendanceBase):
    user_id: Optional[int] = None

class AttendanceUpdate(AttendanceBase):
    pass

class AttendanceBaseInDB(AttendanceBase):
    id: int

    class Config:
        orm_mode = True
        
class Attendance(AttendanceBaseInDB):
    pass

class AttendanceInDB(AttendanceBaseInDB):
    pass