from typing import Optional
from pydantic import BaseModel

class UserBase(BaseModel):
    name: str
    email: str
    password: str
    is_active: Optional[bool] = None

class UserBaseInDB(UserBase):
    id: int = None

    class Config:
        orm_mode = True

class UserCreate(UserBaseInDB):
    name: str
    email: str
    password: str

class UserUpdate(UserBaseInDB):
    password: str

class User(UserBaseInDB):
    pass

class UserInDB(UserBaseInDB):
    password: str