from typing import Optional
from pydantic import BaseModel

class CustomerBase(BaseModel):
    cnpj: str
    name: str
    last_name: Optional[str] = None
    email: str
    address: Optional[str]
    phone: str    
    is_active: bool = None

class CustomerCreate(CustomerBase):
    pass

class CustomerUpdate(CustomerBase):
    pass

class CustomerBaseInDB(CustomerBase):
    id: int

    class Config:
        orm_mode = True
        
class Customer(CustomerBaseInDB):
    pass

class CustomerInDB(CustomerBaseInDB):
    pass