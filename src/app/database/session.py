from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from starlette.requests import Request
from app.config.constants import DATABASE_URL
from app.database.base import Base

engine = create_engine(DATABASE_URL, pool_pre_ping=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base.metadata.create_all(bind=engine)

def get_db(request: Request):
    return request.state.db
