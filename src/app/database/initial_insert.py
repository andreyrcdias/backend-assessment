import logging
from app.data.schema.permission import PermissionCreate
from app.data.schema.user_group import UserGroupCreate
from app.data.schema.user import UserCreate
from app.service.permission import permission_service
from app.service.user_group import user_group_service
from app.service.user import user_service
from app.database.session import db_session
from app.database.base import Base
from app.database.session import engine

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def init_db():
    logger.info('initializating database...')
    Base.metadata.create_all(bind=engine)
    permission = create_permissions()
    user = create_users()
    user_group = create_user_groups(permission_id=permission.id, user_id=user.id)

def create_permissions():
    logger.info('creating permissions...')
    input_obj_perm_create = PermissionCreate(name="PERMISSION_FULL")
    return  permission_service.save(db_session=db_session, obj_input=input_obj_perm_create)

def create_users():
    logger.info('creating users...')
    input_obj_user_create = UserCreate(
        name="USER_ORDER_ANALYST",
        email="order_analyst@banquev.com",
        password="user123"
    )
    saved = user_service.save_hashed_pass(db_session=db_session, obj_input=input_obj_user_create)
    return saved

def create_user_groups(permission_id: int, user_id: int):
    logger.info('creating user_groups...')
    input_obj_user_group = UserGroupCreate(name="ORDER_ANALYST", permission_id=permission_id, user_id=user_id)
    return user_group_service.save(db_session=db_session, obj_input=input_obj_user_group)


