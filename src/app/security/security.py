import jwt
from jwt import PyJWTError
from fastapi import Depends, HTTPException, Security
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from starlette.status import HTTP_403_FORBIDDEN
from app.service.user import user_service
from app.database.session import get_db
from app.config.constants import ALGORITHM, SECRET_KEY
from app.data.model.user import User
from app.data.schema.token import TokenPayload

reusable_oauth2 = OAuth2PasswordBearer(tokenUrl="/api/login/access-token")

def get_current_user(db: Session = Depends(get_db), token: str = Security(reusable_oauth2)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        token_data = TokenPayload(**payload)
    except PyJWTError:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="error on validate credentials")
    user = user_service.find_by_id(db_session=db, id=token_data.user_id)
    if not user:
        raise HTTPException(status_code=404, detail="user not found")
    return user

def get_current_by_permission(current_user: User = Security(get_current_user)):
    
    if len(current_user.user_groups) > 0:
        permission_id = current_user.user_groups[0].permission_id
        permission_found = permission_service.find_by_id(id=permission_id)
        if not permission_found.name == "PERMISSION_FULL":
            raise HTTPException(
            status_code=400, detail="user without privileges"
        )
    return current_user



