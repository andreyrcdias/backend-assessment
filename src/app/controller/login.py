from datetime import timedelta
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from app.service.user import user_service
from app.database.session import get_db
from app.security.security import get_current_user
from app.security.jwt import create_access_token
from app.data.model.user import User as DBUser
from app.data.schema.message import Message
from app.data.schema.token import Token
from app.data.schema.user import User
from app.config.constants import ACCESS_TOKEN_EXPIRE_MINUTES

router = APIRouter()

@router.post("/access-token", response_model=Token, tags=["login"])
def login_access_token(db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):

    user = user_service.authenticate(db, email=form_data.username, password=form_data.password)
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect email or password")
    elif not user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": create_access_token(
            data={"user_id": user.id}, expires_delta=access_token_expires
        ),
        "token_type": "bearer",
    }
