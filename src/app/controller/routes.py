from fastapi import APIRouter
from app.controller import  attendance, catalog, customer, login, order, permission, product, stock, user_group, user

api_router = APIRouter()
api_router.include_router(login.router, prefix="/login", tags=["login"])
api_router.include_router(attendance.router, prefix="/attendances", tags=["attendances"])
api_router.include_router(catalog.router, prefix="/catalogs", tags=["catalogs"])
api_router.include_router(customer.router, prefix="/customers", tags=["customers"])
api_router.include_router(order.router, prefix="/orders", tags=["orders"])
api_router.include_router(permission.router, prefix="/permissions", tags=["permissions"])
api_router.include_router(product.router, prefix="/products", tags=["products"])
api_router.include_router(stock.router, prefix="/stocks", tags=["stocks"])
api_router.include_router(user_group.router, prefix="/usergroups", tags=["usergroups"])
api_router.include_router(user.router, prefix="/users", tags=["users"])