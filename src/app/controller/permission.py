from fastapi import APIRouter, Depends, HTTPException
from typing import List
from sqlalchemy.orm import Session
from app.database.session import get_db
from app.data.schema.permission import Permission, PermissionCreate, PermissionUpdate
from app.data.model.user import User as DBUser
from app.service.permission import permission_service as service
from app.security.security import get_current_user

router = APIRouter()

@router.get("", response_model=List[Permission], tags=["permissions"])
def find_all(db: Session = Depends(get_db), current_user: DBUser = Depends(get_current_user)):
    return service.find_all(db_session=db)

@router.post("", response_model=Permission, tags=["permissions"])
def save(*, db: Session = Depends(get_db), payload: PermissionCreate, current_user: DBUser = Depends(get_current_user)):
    return service.save(db_session=db, obj_input=payload)

@router.get("/{id}", response_model=Permission, tags=["permissions"])
def find_by_id(*, db: Session = Depends(get_db), id: int, current_user: DBUser = Depends(get_current_user)):
    found = service.find_by_id(db_session=db, id=id)
    if not found:
        raise HTTPException(status_code=404, detail="permission not found")
    return found

@router.put("/{id}", response_model=Permission, tags=["permissions"])
def update(*, db: Session = Depends(get_db), id: int, payload: PermissionUpdate, current_user: DBUser = Depends(get_current_user)):
    found = service.find_by_id(db_session=db, id=id)
    if not found:
        raise HTTPException(status_code=404, detail="permission not found")
    return service.update(db_session=db, db_obj=found, obj_input=payload)

@router.delete("/{id}", response_model=Permission, tags=["permissions"])
def delete(*, db: Session = Depends(get_db), id: int, current_user: DBUser = Depends(get_current_user)):
    found = service.find_by_id(db_session=db, id=id)
    if not found:
        raise HTTPException(status_code=404, detail="permission not found")
    return service.delete(db_session=db, id=id)